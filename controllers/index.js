'use strict'

const swaggerUi = require('swagger-ui-express')
const swaggerParser = require('swagger-parser')
const jsYaml = require('js-yaml')

const db = require('../models/index')

const attribute = require('./attribute')
const schema = require('./schema')
const entity = require('./entity')
const openapi = require('../helpers/openapi')
const appErrorSpec = require('../helpers/appErrorSpec')

module.exports.init = async (app) => {
    openapi.init(app, { openapi: "3.0.2", info: { title: 'Backend test task', version: '0.0.1' } })

    openapi.register(appErrorSpec)
    openapi.register(db.Attribute.spec())
    openapi.register(db.Schema.spec())
    openapi.register(db.Entity.spec())

    openapi.register(attribute.getAll)
    openapi.register(attribute.create)
    openapi.register(attribute.get)

    openapi.register(schema.getAll)
    openapi.register(schema.create)
    openapi.register(schema.get)

    openapi.register(entity.getAll)
    openapi.register(entity.create)
    openapi.register(entity.get)
    openapi.register(entity.delete)

    openapi.register(entity.getAllRevisions)
    openapi.register(entity.update)
    openapi.register(entity.getRevision)

    await openApiMagic(app)
}

async function openApiMagic(app) {
    app.use('/docs', swaggerUi.serve)
    app.get('/docs', swaggerUi.setup(openapi.openapiSpec()))
    app.get('/spec', (req, res) => {
        res.setHeader('Access-Control-Allow-Origin', 'https://editor.swagger.io')
        res.setHeader('content-type', 'text/plain')

        res.send(jsYaml.dump(openapi.openapiSpec()))
    })
    app.get('/editSpec', (req, res) => {
        res.setHeader('Location', `https://editor.swagger.io/?url=${req.protocol}://${req.host}:3000/spec`)
        res.sendStatus(302)
    })

    await swaggerParser.validate(openapi.openapiSpec())
}
