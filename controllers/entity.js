'use strict'

const db = require('../models/index')
const AppError = require('../helpers/appError')

module.exports.getAll = {
    spec: `
      /schemas/{schemaName}/entities:
        get:
          tags:
            - entities
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      $ref: '#/components/schemas/entity'
    `,
    handler: async (req, res) => {
        res.json(await db.Entity.getAllLatestRevisions())
    }
}

module.exports.create = {
    spec: `
      /schemas/{schemaName}/entities:
        post:
          tags:
            - entities
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/entity'
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: object
    `,
    handler: async (req, res) => {
        const newEntity = {
            SchemaName: req.params.schemaName,
            value: req.body.value
        }

        const created = await db.Entity.create(newEntity)

        res.json(created)
    }
}

module.exports.get = {
    spec: `
      /schemas/{schemaName}/entities/{entityId}:
        get:
          tags:
            - entities
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
            - in: path
              name: entityId
              schema:
                type: string
                format: uuid
              required: true
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/entity'
            '404':
              description: Not found
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/error'
    `,
    handler: async (req, res) => {
        const entityWithLatestRevision = await db.Entity.getLatestRevision(req.params.entityId)

        if (!entityWithLatestRevision) {
            throw new AppError(404, `Not found`)
        }
    
        res.json(entityWithLatestRevision)
    }
}

module.exports.delete = {
    spec: `
      /schemas/{schemaName}/entities/{entityId}:
        delete:
          tags:
            - entities
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
            - in: path
              name: entityId
              schema:
                type: string
                format: uuid
              required: true
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: object
    `,
    handler: async (req, res) => {
        await db.Entity.destroy({
            where: {
                SchemaName: req.params.schemaName,
                entityId: req.params.entityId
            }
        })

        res.json({})
    }
}

module.exports.getAllRevisions = {
    spec: `
      /schemas/{schemaName}/entities/{entityId}/revisions:
        get:
          tags:
            - entity revisions
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
            - in: path
              name: entityId
              schema:
                type: string
                format: uuid
              required: true
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      $ref: '#/components/schemas/entity'
    `,
    handler: async (req, res) => {
        res.json(await db.Entity.findAll({
            where: {
                SchemaName: req.params.schemaName,
                entityId: req.params.entityId
            }
        }))
    }
}

module.exports.update = {
    spec: `
      /schemas/{schemaName}/entities/{entityId}/revisions/{revision}:
        post:
          tags:
            - entity revisions
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
            - in: path
              name: entityId
              schema:
                type: string
                format: uuid
              required: true
            - in: path
              name: revision
              schema:
                type: string
                pattern: '^[0-9]*$'
              required: true
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/entity'
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/entity'
            '404':
              description: Not found
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/error'
    `,
    handler: async (req, res) => {
        const newEntity = {
            SchemaName: req.params.schemaName,
            entityId: req.params.entityId,
            revision: parseInt(req.params.revision) + 1,
            value: req.body.value
        }

        try {
            const created = await db.Entity.create(newEntity)            
            res.json(created)
        } catch (e) {
            if (e.name === "SequelizeUniqueConstraintError") {
                throw new AppError(400, `Revision '${req.params.revision}' is not the latest`)
            }

            throw e
        }
    }
}

module.exports.getRevision = {
    spec: `
      /schemas/{schemaName}/entities/{entityId}/revisions/{revision}:
        get:
          tags:
            - entity revisions
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
              example: Person
            - in: path
              name: entityId
              schema:
                type: string
                format: uuid
              required: true
            - in: path
              name: revision
              schema:
                type: string
                pattern: '^[0-9]*$'
              required: true
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/entity'
            '404':
              description: Not found
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/error'
    `,
    handler: async (req, res) => {
        const entityRevision = await db.Entity.findOne({
            where: {
                SchemaName: req.params.schemaName,
                entityId: req.params.entityId,
                revision: req.params.revision
            }
        })
    
        if (!entityRevision) {
            throw new AppError(404, `Not found`)
        }
    
        res.json(entityRevision)
    }
}
