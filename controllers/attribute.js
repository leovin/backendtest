'use strict'

const db = require('../models/index')
const AppError = require('../helpers/appError')

module.exports.getAll = {
    spec: `
      /attributes:
        get:
          tags:
            - attributes
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      $ref: '#/components/schemas/attribute'
    `,
    handler: async (req, res) => {
        res.json(await db.Attribute.findAll())
    }
}

module.exports.create = {
    spec: `
      /attributes:
        post:
          tags:
            - attributes
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/attribute'
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/attribute'
    `,
    handler: async (req, res) => {
        try {
            const created = await db.Attribute.create(req.body)
            res.json(created)
        } catch (e) {
            if (e.name === "SequelizeUniqueConstraintError") {
                throw new AppError(400, `Attribute with name '${req.body.name}' already exists`)
            }

            throw e
        }
    }
}

module.exports.get = {
    spec: `
      /attributes/{attributeName}:
        get:
          tags:
            - attributes
          parameters:
            - in: path
              name: attributeName
              schema:
                type: string
              required: true
              example: email
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      $ref: '#/components/schemas/attribute'
            '404':
              description: Not found
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/error'
    `,
    handler: async (req, res) => {
        const attribute = await db.Attribute.findByPk(req.params.attributeName)

        if (!attribute) {
            throw new AppError(404, `Not found`)
        }
    
        res.json(attribute)
    }
}
