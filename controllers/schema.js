'use strict'

const db = require('../models/index')
const AppError = require('../helpers/appError')

module.exports.getAll = {
    spec: `
      /schemas:
        get:
          tags:
            - schemas
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: array
                    items:
                      $ref: '#/components/schemas/schema'
    `,
    handler: async (req, res) => {
        res.json(await db.Schema.findAll())
    }
}

module.exports.get = {
    spec: `
      /schemas/{schemaName}:
        get:
          tags:
            - schemas
          parameters:
            - in: path
              name: schemaName
              schema:
                type: string
              required: true
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/schema'
            '404':
              description: Not found
              content:
                application/json:
                  schema:
                    $ref: '#/components/schemas/error'
    `,
    handler: async (req, res) => {
        const schema = await db.Schema.findByPk(req.params.schemaName)

        if (!schema) {
            throw new AppError(404, `Not found'`)
        }
    
        res.json(schema)
    }
}

module.exports.create = {
    spec: `
      /schemas:
        post:
          tags:
            - schemas
          requestBody:
            required: true
            content:
              application/json:
                schema:
                  $ref: '#/components/schemas/schema'
          responses:
            '200':
              description: success
              content:
                application/json:
                  schema:
                    type: object
    `,
    handler: async (req, res) => {
        try {
            const created = await db.Schema.create(req.body)
            res.json(created)
        } catch (e) {
            if (e.name === "SequelizeUniqueConstraintError") {
                throw new AppError(400, `Schema with name '${req.body.name}' already exists`)
            }

            throw e
        }
    }
}
