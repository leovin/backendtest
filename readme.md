Backend test task implementation
================================

# [original task](original-en.md) ( [оригінал завдання](original-ua.md) )

# how to start
0. docker-compose up
1. npm i
2. node app.js
3. open http://localhost:3000/docs in browser

# key assumptions
* test task description is complete (i.e. there’s no need to tackle performance, scalability, security, etc.)
* ajv and json schema can be used instead of custom schema and validator
* schema versioning is not needed
* entities can be stored in denormalized form and text format istead of normalized and hierarchical

# key decisions
* use ORM but without migrations
* “insert-only” data model for audit and concurrent updates
* OpenAPI for API input parameters validation
