# Backend Test task

Create the ability to describe arbitrary attribute entities and validation for these attributes (the relationship between attributes and entities many-to-many).
The description should be implemented using a DSL or a configuration object (for example, yaml).
Realize CRUD for these objects through REST.

For example, describing the entity `Person' with attributes may look like DSL

```js
define('attribute', 'firstName')
  .validation({ required: true })
define('attribute', 'lastName')
  .validation({ required: true })
define('attribute', 'age')
  .type('int')
  .validation({ min: 1, max: 120 })
define('attribute', 'email')
  .validation({ email: true })

define('entity', 'Person')
  .has('firstName')
  .has('lastName')
  .has('age')
  .hasMany('email') // може мати багато емейлів
```

CRUD in essence may look like this:

```js
const person = new Entity('Person', {
  firstName: 'John',
  lastName: 'Doe',
  age: 10,
  email: [ // only unique emails
    'john.doe@example.come',
    'johnny@gmail.come'
  ]
})

person.validate() // validates all attributes if something is wrong throws exception
person.save() // saves person into db

// Update
person.firstName = 'Bred'
person.save()

// Delete
Entity.destroy('Person', personId)

// Read
Entity.findById('Person', personId)
Entity.findAll('Person')
```

## Basic requirements

1. Use SQL database (we use PostgreSQL)
2. It should be possible to change the entity schema without changing the database schema
3. Use software design patterns to complete the task
4. If you pass attributes that do not exist in the description of the entity, an error is thrown
5. Correctly handle errors, separate client and server errors
6. REST APIs should follow best practices including the correct issuance of status codes

## Bonus

1. Take into account the resolution of conflicts when API is used by several people at the same time and try to update one and the same entity at about the same time.
2. Keep history on changes in entities
3. To have the opportunity to get the state of arbitrary entity / entities for a specified time interval
4. Validation of attributes must be implemented