'use strict'

module.exports = {
    dbConnString: process.env.DB_CONN_STR || "mysql://user:pass@localhost/test",
    reCreateDb: process.env.RECREATE_DB || true
}
