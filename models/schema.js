'use strict'

const Sequelize = require('sequelize')

const validator = require('../helpers/validator')

class Schema extends Sequelize.Model {
    static associate(models) {
        Schema.hasMany(models.Entity)
    }    
    async validate() {
        await validator.validateSchema(this.value)
    }
    static spec() { return { spec: `
      schema:
        type: object
        properties:
          name:
            type: string
          value:
            type: object
    `}}    
}

module.exports = (sequelize, Sequelize) => {
    Schema.init({
        name: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        value: {
            type: Sequelize.TEXT,
            allowNull: false,
            get() {
                return JSON.parse(this.getDataValue('value'))
            },
            set(val) {
                this.setDataValue('value', JSON.stringify(val))
            }
        },
    }, { sequelize })

    return Schema
}
