'use strict'

const Sequelize = require('sequelize')

const validator = require('../helpers/validator')
const attributesSchema = require('../helpers/attributesSchema')

class Attribute extends Sequelize.Model {
    validate() {
        const attribute = {}
        attribute[this.name] = this.value
        validator.validateAttributesSchema({ definitions: attribute })
    }
    static spec() { return { spec: `
      attribute:
        type: object
        properties:
          name:
            type: string
          value:
            type: object
    `}}
}

module.exports = (sequelize, Sequelize) => {
    Attribute.init({
        name: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        value: {
            type: Sequelize.TEXT,
            allowNull: false,
            get() {
                return JSON.parse(this.getDataValue('value'))
            },
            set(val) {
                this.setDataValue('value', JSON.stringify(val))
            }
        },
    }, {
        sequelize,
        hooks: {
            beforeCreate: (attr, options) => {
                attributesSchema.addAttribute(attr.name, attr.value)
            },
            beforeBulkCreate: (attrs, options) => {
                for (let attr of attrs) {
                    attributesSchema.addAttribute(attr.name, attr.value)
                }
            }
        }
    })

    return Attribute
}
