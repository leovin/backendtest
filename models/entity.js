'use strict'

const Sequelize = require('sequelize')

const validator = require('../helpers/validator')

// query builder would be nice instead of hard-coded queries
const getAllQuery = `
SELECT a.*
FROM Entities a
LEFT OUTER JOIN Entities b
    ON a.entityId = b.entityId AND a.revision < b.revision
WHERE b.entityId IS NULL AND a.deletedAt IS NULL
`

class Entity extends Sequelize.Model {
    static associate(models) {
        Entity.belongsTo(models.Schema)
    }
    async validate() {
        const schema = (await this.getSchema()).value

        await validator.validateData(schema, this.value)
    }
    static async getAllLatestRevisions() {    
        return await this.sequelize.query(getAllQuery, {
            model: Entity,
            mapToModel: true
        })
    }
    static async getLatestRevision(entityId) {
        const query = getAllQuery + ` AND a.entityId = '${entityId}'`
        const latestRevisions = await this.sequelize.query(query, {
            model: Entity,
            mapToModel: true
        })

        return latestRevisions[0]
    }
    static spec() { return { spec: `
      entity:
        type: object
        properties:
          entityId:
            type: string
            format: uuid
          revision:
            type: integer
          value:
            type: object
    `}}
}

module.exports = (sequelize) => {
    Entity.init({
        entityId: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey: true
        },
        revision: {
            type: Sequelize.INTEGER,
            primaryKey: true
        },
        value: {
            type: Sequelize.TEXT,
            allowNull: false,
            get() { return JSON.parse(this.getDataValue('value')) },
            set(val) { this.setDataValue('value', JSON.stringify(val)) }
        }
    }, {
        sequelize,
        paranoid: true,
        hooks: {
            beforeCreate: (entity, options) => {
                entity.revision = 0
            },
            beforeBulkCreate: (entities, options) => {
                for (let entity of entities) {
                    entity.revision = 0
                }
            },
            beforeUpdate: (entity, options) => {
                throw new Error("Update is not supported")
            },
            beforeBulkUpdate: (entity, options) => {
                throw new Error("Update is not supported")
            }
        }
    })

    return Entity
}
