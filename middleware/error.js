'use strict'

module.exports = (err, req, res, next) => {
    console.log(err)

    let message = err.message
    let status = err.status || 500

    res.status(status).json({ message })
}
