(async () => {
    try {
        const app = require('express')()
        const bodyParser = require('body-parser')

        const db = require('./models/index')
        const seed = require('./seed')
        const config = require('./config')
        const controllers = require('./controllers/index')
        const attributesSchema = require('./helpers/attributesSchema')
        const errorMiddleware = require('./middleware/error')

        app.use(bodyParser.json())
    
        if (config.reCreateDb) {
            await db.sequelize.sync({ force: true })
            await seed(db)    
        } else {
            await db.sequelize.sync()
        }
        await attributesSchema.init()
    
        await controllers.init(app)
    
        app.use(errorMiddleware)
    
        app.listen(3000)
    } catch (e) {
        console.log("Failed to start", e.stack)
        process.exit(1)
    }
})()
