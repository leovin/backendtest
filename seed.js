'use strict'

module.exports = async (db) => {
    await db.Attribute.bulkCreate([
        { name: "firstName", value: { type: "string" }},
        { name: "lastName", value: { type: "string" }},
        { name: "age", value: {
            type: "integer",
            minimum: 1,
            maximum: 120
        }},
        { name: "email", value: {
            type: "string",
            format: "email"
        }}
    ])

    const schema = await db.Schema.create({
        name: "Person",
        value: {
            type: "object",
            properties: {
                firstName: { $ref: "#/definitions/firstName" },
                lastName: { $ref: "#/definitions/lastName" },
                age: { $ref: "#/definitions/age" },
                email: {
                    type: "array",
                    uniqueItems: true,
                    items: { $ref: "#/definitions/email" }
                },
            },
            required: ["firstName", "lastName"]
        }
    })

    const entity = await db.Entity.create({
        SchemaName: schema.name,
        value: {
            firstName: 'John',
            lastName: 'Doe',
            age: 10,
            email: [
                'john.doe@example.come',
                'johnny@gmail.come'
            ]
        }
    })

    await db.Entity.create({
        SchemaName: schema.name,
        entityId: entity.entityId,
        revision: entity.revision + 1,
        value: {
            firstName: 'Bred',
            lastName: 'Doe',
            age: 10,
            email: [
                'john.doe@example.come',
                'johnny@gmail.come'
            ]
        }
    })
}
