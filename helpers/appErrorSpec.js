module.exports = {
    spec: `
      error:
        type: object
        properties:
          message:
            type: string
        description: |
          HTTP status codes

          400 - Request validation error

          404 - Resource not found

          460-479 - Errors specific to API route

          480-499 - Generic errors not specific to API route
    `
}