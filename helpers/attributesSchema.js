'use strict'

const AppError = require('../helpers/appError')

const attributes = {}

module.exports.init = async () => {
    const db = require('../models/index')
    const attrs = await db.Attribute.findAll({})

    for (let attr of attrs) { attributes[attr.name] = attr.value }
}

module.exports.addAttribute = (name, value) => {
    if (attributes.hasOwnProperty(name)) {
        throw new AppError(400, `Attribute with name '${name}' already exists`)
    }

    attributes[name] = value
}

module.exports.getAttributes = () => attributes
