'use strict'

const ajv = require('ajv')({ allErrors: true, strictKeywords: true })

const attributesSchema = require('../helpers/attributesSchema')
const AppError = require('../helpers/appError')

module.exports.validateData = async (schema, data) => {
    schema.additionalProperties = false

    schema.definitions = attributesSchema.getAttributes()

    const valid = ajv.validate(schema, data)

    if (!valid) throw new AppError(400, JSON.stringify(ajv.errors))
}

module.exports.validateSchema = async (schema) => {
    if (schema.definitions) {
        throw new AppError(400, "Attributes definition in schema is not allowed")
    }

    schema.definitions = attributesSchema.getAttributes()

    ajv.compile(schema)
}

module.exports.validateAttributesSchema = async (schema) => {    
    ajv.compile(schema)
}
