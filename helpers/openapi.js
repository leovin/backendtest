'use strict'

const yaml = require('js-yaml')
const { OpenApiValidator } = require("express-openapi-validate")

let app
let openapiSpec

module.exports.init = (aApp, specDef) => {
    app = aApp
    openapiSpec = specDef
    openapiSpec.paths = {}
    openapiSpec.components = {}
    openapiSpec.components.schemas = {}
}

module.exports.register = (...definitions) => {
    for (let definition of definitions) {
        if (definition.handler) {
            const routeOpenapiSpec = yaml.load(definition.spec)
            
            Object.keys(routeOpenapiSpec).forEach((path) => {
                if (!openapiSpec.paths[path]) openapiSpec.paths[path] = {}
                
                Object.keys(routeOpenapiSpec[path]).forEach((method) => {
                    if (!openapiSpec.paths[path][method]) openapiSpec.paths[path][method] = {}

                    Object.assign(openapiSpec.paths[path][method], routeOpenapiSpec[path][method])

                    let expressPath = path
                    while (expressPath.indexOf('{') > -1) {
                        expressPath = expressPath.replace('{', ':').replace('}', '')
                    }

                    app[method](expressPath,
                        new OpenApiValidator(openapiSpec, { ajvOptions: { allErrors: true, strictKeywords: true } })
                            .validate(method, path),
                        async (req, res, next) => { try { await definition.handler(req, res) } catch (e) { next(e) } })
                })
            })
        } else {
            const modelOpenapiSpec = yaml.load(definition.spec)
            Object.keys(modelOpenapiSpec).forEach((schema) => {
                openapiSpec.components.schemas[schema] = {}
                Object.assign(openapiSpec.components.schemas[schema], modelOpenapiSpec[schema])
            })
        }
    }
}

module.exports.openapiSpec = () => openapiSpec
